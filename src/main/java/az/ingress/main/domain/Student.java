package az.ingress.main.domain;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import  org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.io.Serializable;
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "STUDENT")
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
@Cache(region = "studentCache", usage = CacheConcurrencyStrategy.READ_WRITE)
public class Student implements Serializable {
    private static final long serialVersionUID = 46417994311L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    Long id;

    @Column(name = "NAME")
    String name;

    @Column(name = "SURNAME")
    String surname;

    @Column(name = "AGE")
    Integer age;


}
